package mx.baz.iyc.sdc.clasificador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@ComponentScan("mx.baz")
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
//@EnableScheduling
public class ClasificadorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClasificadorApplication.class, args);
		String puerto = args[args.length - 1].split("=")[1];
		System.setProperty("puertoProperty", puerto);
	}
}
