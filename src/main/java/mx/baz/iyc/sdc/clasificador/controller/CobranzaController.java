package mx.baz.iyc.sdc.clasificador.controller;

import mx.baz.iyc.responsecontainer.model.ResponseService;
import mx.baz.iyc.sdc.clasificador.dao.CobranzaDAO;
import mx.baz.iyc.sdc.clasificador.model.Cobranza;
import mx.baz.iyc.sdc.clasificador.utilerias.constantes.Endpoint;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.CodigoResponse;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.Folio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
public class CobranzaController {
    @Autowired
    private CobranzaDAO cobranzaDAO;

    @Autowired
    private Folio folio;

    //Nuevo
    @RequestMapping(method = RequestMethod.GET, value = Endpoint.PRUEBAS, produces = MediaType.APPLICATION_JSON_VALUE)
    private Object obtenerCobranzaPorGerenciaCorrienteNuevo() throws Exception {
        Cobranza cobTotal = cobranzaDAO.obtenerCobranzaPorGerenciaCorriente(8584);

        return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_200, folio.getFolio(), Arrays.asList(cobTotal)),CodigoResponse.CODIGO_200.getHttpStatus());
    }

}

