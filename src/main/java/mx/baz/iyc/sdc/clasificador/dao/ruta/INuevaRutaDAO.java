package mx.baz.iyc.sdc.clasificador.dao.ruta;

import mx.baz.iyc.sdc.clasificador.bean.nuevaRuta.AccionesRuta;
import mx.baz.iyc.sdc.clasificador.bean.nuevaRuta.IdentificadorArbol;
import mx.baz.iyc.sdc.clasificador.bean.segmentacion.CadArbol;

import java.util.List;

public interface INuevaRutaDAO {
  List<AccionesRuta> obtenerAcciones(String noEmpleado) throws Exception;
  List<IdentificadorArbol> obtenerIdArboles(String noEmpleado) throws Exception;
  CadArbol obtenerArbol(int IdArbol) throws Exception;
}
