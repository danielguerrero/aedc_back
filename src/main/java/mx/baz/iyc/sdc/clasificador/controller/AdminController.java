package mx.baz.iyc.sdc.clasificador.controller;

import mx.baz.iyc.responsecontainer.model.ResponseService;
import mx.baz.iyc.sdc.clasificador.dao.admin.ObtenerMarcasDAO;
import mx.baz.iyc.sdc.clasificador.dao.admin.UnidadesNegocioDAO;
import mx.baz.iyc.sdc.clasificador.model.admin.SalidaMarcas;
import mx.baz.iyc.sdc.clasificador.model.admin.SalidaMarcasAsociadas;
import mx.baz.iyc.sdc.clasificador.model.admin.SalidaUnidadesNegocio;
import mx.baz.iyc.sdc.clasificador.utilerias.constantes.Endpoint;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.CodigoResponse;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.Folio;
import mx.baz.iyc.service.EscribirLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @author 202952 - Daniel Guerrero - dguerreror@elektra.com.mx
 * @since 18/MAYO/2021
 */

@RestController
public class AdminController {
    @Autowired
    private Folio folio;

    @Autowired
    private EscribirLog escribirLog;

    @Autowired
    private UnidadesNegocioDAO unidadesNegocioDAO;

    @Autowired
    private ObtenerMarcasDAO obtenerMarcasDAO;

    @GetMapping(
            value = Endpoint.OBTENER_UNIDADES_DE_NEGOCIO_POR_USUARIO,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Object obtenerUnidadesNegocioPorUsuario(@PathVariable String noEmpleado) throws Exception{
        List<SalidaUnidadesNegocio> u = unidadesNegocioDAO.obtenerUnidadesNegocio(noEmpleado);
        return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_200, folio.getFolio(), Arrays.asList(u)),CodigoResponse.CODIGO_200.getHttpStatus());
    }

    @GetMapping(
            value = Endpoint.OBTENER_MARCASUNIDAD,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Object obtenerMarcasAsociadas() throws Exception{
        List<SalidaMarcasAsociadas> s = obtenerMarcasDAO.obtenerMarcasAsociadasPorUnidadNegocio();
        return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_200, folio.getFolio(), Arrays.asList(s)),CodigoResponse.CODIGO_200.getHttpStatus());
    }
    @GetMapping(
            value = Endpoint.OBTENER_MARCAS,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Object ObtenerMarcas() throws  Exception {
        List<SalidaMarcas> m = obtenerMarcasDAO.obtenerMarcas();
        return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_200, folio.getFolio(), Arrays.asList(m)),CodigoResponse.CODIGO_200.getHttpStatus());
    }
}
