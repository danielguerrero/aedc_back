package mx.baz.iyc.sdc.clasificador.dao.admin;

import mx.baz.iyc.enums.Nivel;
import mx.baz.iyc.sdc.clasificador.config.db.Functions;
import mx.baz.iyc.sdc.clasificador.config.db.OracleDao;
import mx.baz.iyc.sdc.clasificador.mappers.InsertarArbolRowMapper;
import mx.baz.iyc.sdc.clasificador.mappers.admin.UnidadesNegocioMapper;
import mx.baz.iyc.sdc.clasificador.model.SalidaEstandar;
import mx.baz.iyc.sdc.clasificador.model.admin.SalidaUnidadesNegocio;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.Folio;
import mx.baz.iyc.service.EscribirLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.*;

@Component
public class UnidadesNegocioDAO {

    private final DataSource dataSource;

    @Autowired
    private Folio folio;

    @Autowired
    private EscribirLog escribirLog;

    @Autowired
    public UnidadesNegocioDAO(DataSource dataSource){ this.dataSource = dataSource; }

    public List<SalidaUnidadesNegocio> obtenerUnidadesNegocio(String noEmpleado) throws Exception{
        long idTransaction = folio.getFolioL();
        Map<String, Object> entrada = new LinkedHashMap<>();
        entrada.put("noEmpleado", noEmpleado);

        OracleDao<SalidaUnidadesNegocio> oracleDao = new OracleDao<>();

        escribirLog.escribir(UnidadesNegocioDAO.class, Nivel.INFORMATIVO, "Funcion: " + Functions.OBTENER_UNIDADES_DE_NEGOCIO_POR_USUARIO.getFuncion());
        List<SalidaUnidadesNegocio> salida = oracleDao.call(dataSource, entrada, Functions.OBTENER_UNIDADES_DE_NEGOCIO_POR_USUARIO.getFuncion(), new UnidadesNegocioMapper(), idTransaction);

        return salida;
    }
}
