package mx.baz.iyc.sdc.clasificador.mappers;

import mx.baz.iyc.sdc.clasificador.bean.segmentacion.CadJson;
import mx.baz.iyc.sdc.clasificador.bean.segmentacion.Caracteristicas;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *  Clase que obtiene las características utilizadas para el arbol.
 *  @author 202952 - Emmert - shernandeze@eleketra.com.mx
 *  @since 10-Junio-2021
 */
public class CaracteristicasRowMapper implements RowMapper<Caracteristicas> {

  @Override
  public Caracteristicas mapRow(ResultSet rs, int rowNum) throws SQLException {
    Caracteristicas c1 = new Caracteristicas();
    c1.setId(rs.getInt(1));
    c1.setTipo(rs.getInt(2));
    c1.setName(rs.getString(3));
    return c1;
  }
}
