package mx.baz.iyc.sdc.clasificador.utilerias.enums;

import mx.baz.iyc.responsecontainer.CodigosRespuesta.CodigosRespuesta;
import org.springframework.http.HttpStatus;

public enum CodigoResponse implements CodigosRespuesta {

    CODIGO_200("200.CobranzaCredito-Investigacion-Cobranza.000200", "Proceso terminado correctamente.", HttpStatus.OK),
    CODIGO_400("400.CobranzaCredito-Investigacion-Cobranza.000400", "Datos de entrada incorrectos, favor de validar.", HttpStatus.BAD_REQUEST),
    CODIGO_404("404.CobranzaCredito-Investigacion-Cobranza.000404", "Recurso no encontrado.", HttpStatus.NOT_FOUND),
    CODIGO_405("405.CobranzaCredito-Investigacion-Cobranza-000405", "Metodo no permitido en el recurso", HttpStatus.METHOD_NOT_ALLOWED),
    CODIGO_500("500.CobranzaCredito-Investigacion-Cobranza.000500", "Fallo en el servidor al procesar la petición.",
            HttpStatus.INTERNAL_SERVER_ERROR),;

    private String codigo;
    private String descripcion;
    private HttpStatus httpStatus;

    private CodigoResponse(String codigo, String descripcion, HttpStatus httpStatus) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.httpStatus = httpStatus;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

}
