package mx.baz.iyc.sdc.clasificador.bean.segmentacion;

public class Definicion {
  private int valor;
  private String name;

  public int getValor() {
    return valor;
  }

  public void setValor(int valor) {
    this.valor = valor;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
