package mx.baz.iyc.sdc.clasificador.dao;

import mx.baz.iyc.sdc.clasificador.config.db.Functions;
import mx.baz.iyc.sdc.clasificador.config.db.OracleDao;
import mx.baz.iyc.sdc.clasificador.mappers.CobranzaGerenciaRowMapper;
import mx.baz.iyc.sdc.clasificador.model.Cobranza;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.Folio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Clase que sirve para acceder a los datos de cobranza
 * @author 202952 - Daniel Guerrero - dguerreror@elektra.com.mx
 * @since 03-ABRIL-2020
 */

@Component
public class CobranzaDAO {
    private final DataSource dataSource;

    @Autowired
    private Folio folio;

    @Autowired
    public CobranzaDAO(DataSource dataSource){ this.dataSource = dataSource; }


    public Cobranza obtenerCobranzaPorGerenciaCorriente(int gerencia) throws Exception{
        long idTransaction = folio.getFolioL();
        Map<String, Object> entrada = new LinkedHashMap<>();
        entrada.put("gerencia", gerencia);

        OracleDao<Cobranza> oracleDao = new OracleDao<>();
        List<Cobranza> salida = new ArrayList<>();

        salida = oracleDao.call(dataSource, entrada, Functions.CONSULTA_COBRANZA_GERENCIA.getFuncion(), new CobranzaGerenciaRowMapper(), idTransaction);

        return salida.get(0);
    }

}
