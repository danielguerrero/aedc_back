package mx.baz.iyc.sdc.clasificador.dao;

import mx.baz.iyc.sdc.clasificador.config.db.Functions;
import mx.baz.iyc.sdc.clasificador.config.db.OracleDao;
import mx.baz.iyc.sdc.clasificador.mappers.CobranzaEmpleadoRowMapper;
import mx.baz.iyc.sdc.clasificador.mappers.CobranzaGerenciaRowMapper;
import mx.baz.iyc.sdc.clasificador.mappers.InsertarEmpleadoRowMapper;
import mx.baz.iyc.sdc.clasificador.model.Cobranza;
import mx.baz.iyc.sdc.clasificador.model.GuardarCliente;
import mx.baz.iyc.sdc.clasificador.model.SalidaEstandar;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.Folio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class InsertarDAO {
  private final DataSource dataSource;

  @Autowired
  private Folio folio;

  @Autowired
  public InsertarDAO(DataSource dataSource){ this.dataSource = dataSource; }

  public SalidaEstandar guardarClientes(int idBloque, String cadena, String empleado) throws Exception{
    long idTransaccion = folio.getFolioL();
    Map<String, Object> entrada = new LinkedHashMap<>();
    entrada.put("pa_idbloque", idBloque);
    entrada.put("pa_cadena", cadena);
    entrada.put("pa_user", empleado);

    OracleDao<SalidaEstandar> oracleDao = new OracleDao<>();
    List<SalidaEstandar> salida = new ArrayList<>();
    salida = oracleDao.call(dataSource, entrada, Functions.GUARDAR_CLIENTES.getFuncion(), new InsertarEmpleadoRowMapper(), idTransaccion);

    return salida.get(0);
  }

}
