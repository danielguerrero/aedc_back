package mx.baz.iyc.sdc.clasificador.mappers.admin;

import mx.baz.iyc.sdc.clasificador.model.admin.SalidaMarcas;
import mx.baz.iyc.sdc.clasificador.model.admin.SalidaMarcasAsociadas;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *  @author 202952 - Daniel Guerrero - dguerreror@eleketra.com.mx
 *  @since 29-JUNIO-2021
 */
public class ObtenerMarcasMapper implements RowMapper<SalidaMarcas> {

    @Override
    public SalidaMarcas mapRow(ResultSet rs, int rowNum) throws SQLException {
        SalidaMarcas s = new SalidaMarcas();
        s.setFiid(rs.getInt(1));
        s.setFcnombre(rs.getString(2));
        return s;
    }
}
