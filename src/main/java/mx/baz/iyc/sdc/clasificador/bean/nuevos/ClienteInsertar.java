package mx.baz.iyc.sdc.clasificador.bean.nuevos;

public class ClienteInsertar {
  private int fiidcte;
  private int fnstatus;
  private String clasificaciones;

  public ClienteInsertar(int fiidcte, int fnstatus, String clasificaciones) {
    this.fiidcte = fiidcte;
    this.fnstatus = fnstatus;
    this.clasificaciones = clasificaciones;
  }

  public int getFiidcte() { return fiidcte; }
  public void setFiidcte(int fiidcte) { this.fiidcte = fiidcte; }

  public int getFnstatus() { return fnstatus; }
  public void setFnstatus(int fnstatus) { this.fnstatus = fnstatus; }

  public String getClasificaciones() { return clasificaciones; }
  public void setClasificaciones(String clasificaciones) { this.clasificaciones = clasificaciones; }

  @Override
  public String toString() {
    return "{" +
      "fiidcte:'" + fiidcte + '\'' +
      ", fnstatus:'" + fnstatus + '\'' +
      ", clasificaciones:'" + clasificaciones + '\'' +
      '}';
  }
}
