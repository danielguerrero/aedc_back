package mx.baz.iyc.sdc.clasificador.config.db;

import oracle.jdbc.OracleTypes;
import org.springframework.data.jdbc.support.oracle.SqlStructArrayValue;
import org.springframework.jdbc.core.support.SqlLobValue;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.Date;

/**
 * Realiza la llamada a un StoreProcedure (utilidad).
 * @author 347409 - Ubaldo Torres Juarez - utorresj@elektra.com.mx
 * @modified 202952 - Daniel Guerrero - dguerreror@elektra.com.mx
 * @since 17-ABRIL-2020
 */

public class OracleUtil {
    static int getType(Object value){
        if(value instanceof String)
            return Types.VARCHAR;
        else if(value instanceof Integer)
            return Types.NUMERIC;
        else if(value instanceof Long)
            return Types.NUMERIC;
        else if(value instanceof BigDecimal)
            return Types.NUMERIC;
        else if(value instanceof Date)
            return Types.DATE;
        else if (value instanceof Double)
            return Types.DOUBLE;
        else if(value instanceof SqlStructArrayValue)
            return OracleTypes.ARRAY;
        else if (value instanceof SqlLobValue)
            return Types.BLOB;
        else if(value == null)
            return Types.VARCHAR;
        else
            return -10000;
    }
}
