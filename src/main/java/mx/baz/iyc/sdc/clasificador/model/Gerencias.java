package mx.baz.iyc.sdc.clasificador.model;

public class Gerencias {
    // Entradas
    private int gerencia;

    // Salidas
    private String r_nivel;
    private String r_empnum;
    private String r_empnom;
    private String r_puenum;
    private String r_puenom;
    private String r_depnum;
    private String r_depnom;
    private String r_deptip;
    private String r_pais;
    private String r_paisdesc;

    public int getGerencia() {
        return gerencia;
    }

    public void setGerencia(int gerencia) {
        this.gerencia = gerencia;
    }

    public String getR_nivel() {
        return r_nivel;
    }

    public void setR_nivel(String r_nivel) {
        this.r_nivel = r_nivel;
    }

    public String getR_empnum() {
        return r_empnum;
    }

    public void setR_empnum(String r_empnum) {
        this.r_empnum = r_empnum;
    }

    public String getR_empnom() {
        return r_empnom;
    }

    public void setR_empnom(String r_empnom) {
        this.r_empnom = r_empnom;
    }

    public String getR_puenum() {
        return r_puenum;
    }

    public void setR_puenum(String r_puenum) {
        this.r_puenum = r_puenum;
    }

    public String getR_puenom() {
        return r_puenom;
    }

    public void setR_puenom(String r_puenom) {
        this.r_puenom = r_puenom;
    }

    public String getR_depnum() {
        return r_depnum;
    }

    public void setR_depnum(String r_depnum) {
        this.r_depnum = r_depnum;
    }

    public String getR_depnom() {
        return r_depnom;
    }

    public void setR_depnom(String r_depnom) {
        this.r_depnom = r_depnom;
    }

    public String getR_deptip() {
        return r_deptip;
    }

    public void setR_deptip(String r_deptip) {
        this.r_deptip = r_deptip;
    }

    public String getR_pais() {
        return r_pais;
    }

    public void setR_pais(String r_pais) {
        this.r_pais = r_pais;
    }

    public String getR_paisdesc() {
        return r_paisdesc;
    }

    public void setR_paisdesc(String r_paisdesc) {
        this.r_paisdesc = r_paisdesc;
    }
}
