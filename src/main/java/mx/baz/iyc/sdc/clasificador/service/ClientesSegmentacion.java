package mx.baz.iyc.sdc.clasificador.service;

import mx.baz.iyc.sdc.clasificador.bean.segmentacion.Arbol;
import mx.baz.iyc.sdc.clasificador.bean.segmentacion.Bloque;
import mx.baz.iyc.sdc.clasificador.bean.segmentacion.Caracteristicas;
import mx.baz.iyc.sdc.clasificador.bean.segmentacion.ClientesBean;

import java.util.List;
import java.util.concurrent.Callable;


public interface ClientesSegmentacion {
  Callable<Object> getProcesaArbol();
  boolean segmentaNuevo(Arbol arbol) throws Exception;
  int segmentaNuevoFinal(Arbol arbol) throws Exception;
  String searchHoja (List<Arbol> rama, ClientesBean clien);
  Bloque llenarClientes () throws Exception;
  Arbol obtenerArbol() throws Exception;
  List<Caracteristicas> obtenerCaracteristicas (String noEmpleado) throws Exception;
}
