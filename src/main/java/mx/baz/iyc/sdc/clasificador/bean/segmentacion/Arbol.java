package mx.baz.iyc.sdc.clasificador.bean.segmentacion;

import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;

import java.util.Collections;
import java.util.List;


public class Arbol {
  private String name;
  private int idCaract;
  private int tipo;
  private int[] defCaracterisitica;
  private List<Arbol> children = Collections.emptyList();

  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  public int getIdCaract() {
    return idCaract;
  }
  public void setIdCaract(int idCaract) {
    this.idCaract = idCaract;
  }

  public int getTipo() {
    return tipo;
  }
  public void setTipo(int tipo) {
    this.tipo = tipo;
  }

  public int[] getDefCaracterisitica() {
    return defCaracterisitica;
  }
  public void setDefCaracterisitica(int[] defCaracterisitica) {
    this.defCaracterisitica = defCaracterisitica;
  }

  public List<Arbol> getChildren() {
    return children;
  }
  public void setChildren(List<Arbol> children) {
    this.children = children;
  }
}
