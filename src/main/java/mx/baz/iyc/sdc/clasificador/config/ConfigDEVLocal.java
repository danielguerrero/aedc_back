package mx.baz.iyc.sdc.clasificador.config;

import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * @author 202952 - Daniel Guerrero - dguerreror@elektra.com.mx
 * @since 18/MAYO/2021
 */

@Configuration
@Profile("dev")
public class ConfigDEVLocal {

    @Autowired
    private AppConfig app;

    @Autowired
    private Environment env;

    @Bean
    public DataSource dataSource() throws SQLException {
        final String username = "RCREDITO";
        //final String p = "RCREDITO1";
        final String p = "U5ErBDc0br4nZa1";

        PoolDataSource dataSource = PoolDataSourceFactory.getPoolDataSource();

        dataSource.setUser(username);
        dataSource.setPassword(p);
        dataSource.setConnectionFactoryClassName("oracle.jdbc.pool.OracleDataSource");
        dataSource.setURL("jdbc:oracle:thin:@(DESCRIPTION=" +
                "    (ADDRESS_LIST=" +
                "        (FAILOVER=on)" +
                "        (ADDRESS=(PROTOCOL=TCP)(HOST=10.82.57.245)(PORT=4410))" +
                "    )" +
                "    (CONNECT_DATA=" +
                "        (FAILOVER_MODE=(TYPE=session)(METHOD=basic)(RETRIES=180))" +
                "        (SERVER=dedicated)(SERVICE_NAME=RCBDDES)" +
                "    )" +
                ")");
        dataSource.setFastConnectionFailoverEnabled(true);
        dataSource.setValidateConnectionOnBorrow(true);
        dataSource.setInitialPoolSize(1);
        dataSource.setMinPoolSize(10);
        dataSource.setMaxPoolSize(20);
        return dataSource;
    }
}
