package mx.baz.iyc.sdc.clasificador.utilerias.enums;

import mx.baz.iyc.sdc.clasificador.utilerias.constantes.General;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class Folio {

    public String getFolio() {

        LocalDateTime localDateTime = LocalDateTime.now();

        Pattern p = Pattern.compile("[-:T.]");
        Matcher m = p.matcher(localDateTime.toString());
        String remplazo = null;
        if (m.find()) {
            remplazo = m.replaceAll("");
        }
        return General.COD_APP.concat("-").concat(remplazo.substring(0, 16)).concat("00");

    }

    public Long getFolioL(){
        LocalDateTime localDateTime = LocalDateTime.now();
        Long folio;

        Pattern p = Pattern.compile("[:T.]");
        Matcher m = p.matcher(localDateTime.toString());
        String remplazo = null;

        if(m.find()){
            remplazo = m.replaceAll("");
        }

        String salida = remplazo.substring(0,16).concat("00");
        salida = salida.replaceAll("-", "");

        folio = Long.parseLong(salida);

        return folio;
    }
}
