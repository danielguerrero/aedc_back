package mx.baz.iyc.sdc.clasificador.utilerias.actuator;

import com.google.gson.Gson;
import mx.baz.iyc.enums.Nivel;
import mx.baz.iyc.sdc.clasificador.config.ApplicationSetup;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.CodigoResponse;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.Folio;
import mx.baz.iyc.service.EscribirLog;
import org.apache.commons.net.telnet.TelnetClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@Component
@PropertySource("classpath:application.properties")
public class ApplicationInfo extends AbstractHealthIndicator {

    @Value("${info.application.additional}")
    private String json;

    @Value("${info.application.puerto}")
    private String puerto;

    @Value("${info.application.name}")
    private String name;

    @Value("${info.application.version}")
    private String version;

    @Value("${info.application.encoding}")
    private String encoding;

    @Value("${info.application.java.version}")
    private String javaVersion;

    @Value("${info.application.jvm.buffer.memory.used}")
    private String memoria;

    @Autowired
    private Folio folio;

    @Autowired
    private ApplicationSetup appSetup;

    @Autowired
    private EscribirLog log;

    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        Map<String, Object> fromJson = new HashMap<>();
        Map<String, String> resultado = Map.of("Version", version, "Aplicacion", name);
        fromJson.put("codigo", CodigoResponse.CODIGO_200.getCodigo());
        fromJson.put("mensaje", CodigoResponse.CODIGO_200.getDescripcion());
        fromJson.put("folio", folio.getFolio());
        fromJson.put("resultado", resultado);
        fromJson.put("telnet", obtenerTelnets());
        builder.up().withDetails(fromJson);
        log.escribir(getClass(), Nivel.INFORMATIVO,
                new Gson().toJson(builder.build().getDetails()).replaceAll("\"", "'"));
    }

    private Map<String, Status> obtenerTelnets() {
        Map<String, Status> status = new HashMap<>();
        if (appSetup.getUrls() != null) {
            appSetup.getUrls().forEach((k, v) -> {
                String key = "";
                TelnetClient clientTelnet = new TelnetClient();
                clientTelnet.setConnectTimeout(100);
                URL url = null;
                try {
                    url = new URL(v);
                } catch (MalformedURLException e1) {
                    log.escribir(getClass(), Nivel.INFORMATIVO, e1.getMessage());
                }
                key = url.getHost() + ":" + url.getPort();
                if (url != null && !status.containsKey(key)) {
                    try {
                        clientTelnet.connect(url.getHost(), url.getPort());
                        status.put(key, Status.UP);
                    } catch (Exception e) {
                        status.put(key, Status.DOWN);
                    }
                }
            });
        }
        return status;
    }

}
