package mx.baz.iyc.sdc.clasificador.mappers;

import mx.baz.iyc.sdc.clasificador.model.SalidaEstandar;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *  Clase que mapea el insert del arbol
 *  @author 202952 - Daniel Guerrero - dguerreror@eleketra.com.mx
 *  @since 27-MAYO-2021
 */

public class InsertarArbolRowMapper implements RowMapper<SalidaEstandar> {

  @Override
  public SalidaEstandar mapRow(ResultSet rs, int rowNum) throws SQLException {
    SalidaEstandar c1 = new SalidaEstandar();
    c1.setCodigo(rs.getInt(1));
    c1.setMensaje(rs.getString(2));
    return c1;
  }
}
