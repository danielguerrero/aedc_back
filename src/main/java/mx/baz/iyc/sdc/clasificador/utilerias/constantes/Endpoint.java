package mx.baz.iyc.sdc.clasificador.utilerias.constantes;

public class Endpoint {
    public static final String CTR_ENDPOINT = "/v1";

    public static final String MONITOREO = "/monitoreo";

    public static final String TEST = "/test";

    public static final String V1 = "sdc/v1/inicio";

    public static final String CLASIFICADOR = "sdc/v1/clasificador";

    public static final String V2 = "/persistenciaoracle/v2/query";

    public static final String V3 = "/persistenciaoracle/v3/query";

    public static final String PRUEBAS = "/pruebas/v1/bd";

    public static final String CLIENTES = "/sdc/v1/clientes";

    public static final String SEGMENTACION = "/segmentacion";

    public static final String NUEVA_SEGMENTACION = "/nuevasegmentacion";

    public static final String ARBOL = "/arbol";

    public static final String OBTENER_UNIDADES_DE_NEGOCIO_POR_USUARIO = "/unidadesnegocio/{noEmpleado}";

    public static final String OBTENER_MARCASUNIDAD = "/marcasUnidad";

    public static final String OBTENER_MARCAS = "/marcas";
}
