package mx.baz.iyc.sdc.clasificador.handler;

import mx.baz.iyc.enums.Nivel;
import mx.baz.iyc.responsecontainer.model.ResponseService;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.CodigoResponse;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.Folio;
import mx.baz.iyc.service.EscribirLog;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que personaliza las salidas HTTP en casos de errores o Bad request, esto para cumplir con el contrato de interfaz.
 * @author 347409 - Ubaldo Torres Juarez - utorresj@elektra.com.mx
 * @modified 202952 - Daniel Guerrero - dguerreror@elektra.com.mx
 */

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class CustomHandlerProblems extends ResponseEntityExceptionHandler {

    @Autowired
    private Folio folio;

    @Autowired
    private EscribirLog log;

    @Override
    protected ResponseEntity handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add("El metodo " + ex.getMethod() + " no esta disponible para este recurso");
        details.add("Metodos permitidos: " + ex.getSupportedHttpMethods());
        log.escribir(getClass(), Nivel.INFORMATIVO, "HttpRequestMethodNotSupportedException");
        return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_405, folio.getFolio(), details), CodigoResponse.CODIGO_405.getHttpStatus());
    }

    @Override
    protected ResponseEntity handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        String idTransaction = folio.getFolio();
        List<String> details = new ArrayList<>();
        for(ObjectError error : ex.getBindingResult().getAllErrors()) {
            details.add(error.getDefaultMessage());
        }

        log.escribir(getClass(), Nivel.INFORMATIVO, "MethodArgumentNotValidException");
        return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_400, folio.getFolio(), details), CodigoResponse.CODIGO_400.getHttpStatus());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return super.handleHttpMediaTypeNotSupported(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity handleHttpMediaTypeNotAcceptable(HttpMediaTypeNotAcceptableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        long idTransaction = folio.getFolioL();
        List<String> details = new ArrayList<>();
        details.add("La petición se hizo de manera incorrecta.");

        return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_400, folio.getFolio(), details), CodigoResponse.CODIGO_400.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleMissingPathVariable(MissingPathVariableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return super.handleMissingPathVariable(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return super.handleMissingServletRequestParameter(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return super.handleServletRequestBindingException(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleConversionNotSupported(ConversionNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return super.handleConversionNotSupported(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return super.handleTypeMismatch(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_400, folio.getFolio(), new String("[]")), CodigoResponse.CODIGO_400.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return super.handleHttpMessageNotWritable(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestPart(MissingServletRequestPartException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return super.handleMissingServletRequestPart(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return super.handleBindException(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String idTransaction = folio.getFolio();
        List<String> errores = new ArrayList<>();
        errores.add("Recurso no encontrado: " + ex.getRequestURL());

        return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_404, idTransaction, errores), CodigoResponse.CODIGO_404.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleAsyncRequestTimeoutException(AsyncRequestTimeoutException ex, HttpHeaders headers, HttpStatus status, WebRequest webRequest) {
        return super.handleAsyncRequestTimeoutException(ex, headers, status, webRequest);
    }

    @Override
    protected ResponseEntity handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_500, folio.getFolio(), new String("")), CodigoResponse.CODIGO_500.getHttpStatus());
    }
}