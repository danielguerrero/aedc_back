package mx.baz.iyc.sdc.clasificador.mappers.admin;

import mx.baz.iyc.sdc.clasificador.model.admin.SalidaMarcasAsociadas;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ObtenerMarcasUnidadMapper implements RowMapper<SalidaMarcasAsociadas> {
  @Override
  public SalidaMarcasAsociadas mapRow(ResultSet rs, int rowNum) throws SQLException {
    SalidaMarcasAsociadas s = new SalidaMarcasAsociadas();
    s.setFiid(rs.getString(1));
    s.setFcnombre(rs.getString(2));
    s.setFnmarca(rs.getString(3));
    return s;
  }
}
