package mx.baz.iyc.sdc.clasificador.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "app")
@Data
public class AppConfig {

    private String ambiente;
    private String llavePublica;
    private String name;
    private String usrBD;
    private String pswBD;
}
