package mx.baz.iyc.sdc.clasificador.bean;

public class Salida {
    private String idCliente;
    private String clasificacion;

    public Salida(String idCliente, String clasificacion) {
        this.idCliente = idCliente;
        this.clasificacion = clasificacion;
    }

    public String getIdCliente() { return idCliente; }
    public void setIdCliente(String idCliente) { this.idCliente = idCliente; }

    public String getClasificacion() { return clasificacion; }
    public void setClasificacion(String clasificacion) { this.clasificacion = clasificacion; }
}
