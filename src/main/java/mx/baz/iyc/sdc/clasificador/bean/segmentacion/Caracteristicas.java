package mx.baz.iyc.sdc.clasificador.bean.segmentacion;

import java.util.List;

public class Caracteristicas {
  private int id;
  private int tipo;
  private int activo;
  private String name;
  private List<Definicion> datos;

  public Caracteristicas () {
    this.activo = 0;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getTipo() {
    return tipo;
  }

  public void setTipo(int tipo) {
    this.tipo = tipo;
  }

  public int getActivo() {
    return activo;
  }

  public void setActivo(int activo) {
    this.activo = activo;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Definicion> getDatos() {
    return datos;
  }

  public void setDatos(List<Definicion> datos) {
    this.datos = datos;
  }
}
