package mx.baz.iyc.sdc.clasificador.dao;

import mx.baz.iyc.sdc.clasificador.config.db.Functions;
import mx.baz.iyc.sdc.clasificador.config.db.OracleDao;
import mx.baz.iyc.sdc.clasificador.mappers.LeerClientesRowMapper;
import mx.baz.iyc.sdc.clasificador.model.ClientesDB;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.Folio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class LeerClientesDAO {

  private final DataSource dataSource;

  @Autowired
  private Folio folio;

  @Autowired
  public LeerClientesDAO(DataSource dataSource){ this.dataSource = dataSource; }

  public List<ClientesDB> leerClientesNuevo() throws Exception{
    long idTransaccion = folio.getFolioL();
    Map<String, Object> entrada = new LinkedHashMap<>();

    OracleDao<ClientesDB> oracleDao = new OracleDao<>();
    List<ClientesDB> salida = new ArrayList<>();

    salida = oracleDao.call(dataSource, entrada, Functions.LEER_CLIENTES.getFuncion(), new LeerClientesRowMapper(), idTransaccion);

    return salida;
  }
}
