package mx.baz.iyc.sdc.clasificador.bean;

public class InsertarArbol {
  private int opcion;
  private int fiid;
  private String nombre;
  private String inicio;
  private String fin;
  private int status;
  private String arbol;
  private int tipo;
  private int usuario;

  public int getOpcion() { return opcion; }
  public void setOpcion(int opcion) { this.opcion = opcion; }

  public int getFiid() { return fiid; }
  public void setFiid(int fiid) { this.fiid = fiid; }

  public String getNombre() { return nombre; }
  public void setNombre(String nombre) { this.nombre = nombre; }

  public String getInicio() { return inicio; }
  public void setInicio(String inicio) { this.inicio = inicio; }

  public String getFin() { return fin; }
  public void setFin(String fin) { this.fin = fin; }

  public int getStatus() { return status; }
  public void setStatus(int status) { this.status = status; }

  public String getArbol() { return arbol; }
  public void setArbol(String arbol) { this.arbol = arbol; }

  public int getTipo() { return tipo; }
  public void setTipo(int tipo) { this.tipo = tipo; }

  public int getUsuario() { return usuario; }
  public void setUsuario(int usuario) { this.usuario = usuario; }
}
