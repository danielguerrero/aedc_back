package mx.baz.iyc.sdc.clasificador.proceso;

import mx.baz.iyc.enums.Nivel;
import mx.baz.iyc.sdc.clasificador.bean.segmentacion.Arbol;
import mx.baz.iyc.sdc.clasificador.controller.ClientesController;
import mx.baz.iyc.sdc.clasificador.service.ClientesSegmentacion;
import mx.baz.iyc.sdc.clasificador.utilerias.global.VariablesGlobales;
import mx.baz.iyc.service.EscribirLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Component
public class Proceso {

    @Autowired
    private ClientesSegmentacion clienteSegmenta;

    @Autowired
    private EscribirLog escribirLog;

    private Arbol arbol;

    public void arbol() throws Exception {
        Instant INICIO = Instant.now();

        escribirLog.escribir(ClientesController.class, Nivel.INFORMATIVO, "******************************");

        arbol = clienteSegmenta.obtenerArbol();

        ExecutorService executor = Executors.newCachedThreadPool();
        List<Callable<String>> tareas = new ArrayList<>();

        Callable<String> tarea= ()->{
            try {
                TimeUnit.MILLISECONDS.sleep(300);
                boolean bandera = true;
                procesoVariablesLocales();

                while (bandera){
                    int resultado = clienteSegmenta.segmentaNuevoFinal(arbol);
                    bandera = validarBandera();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //Log("Finaliza la tarea " + numeroTarea);
            return "Resultado de la tarea";
        };

        tareas.add(tarea);
        tareas.add(tarea);
        tareas.add(tarea);
        tareas.add(tarea);

        executor.invokeAll(tareas);
        executor.shutdown();

        String duracion = Duration.between(INICIO, Instant.now()).toString();

        escribirLog.escribir(ClientesController.class, Nivel.INFORMATIVO, "Duraciones: " + duracion);
    }

    public void procesoVariablesLocales(){
        VariablesGlobales.putCache("bandera", 1);
    }

    public boolean validarBandera() {
        Map<String, Integer> map = VariablesGlobales.cache;
        int a = map.get("bandera");
        /*    System.out.println("Valor de bandera: " + a);*/
        if (a == 0) { return false; }

        return true;
    }
}
