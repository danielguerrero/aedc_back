package mx.baz.iyc.sdc.clasificador.bean.nuevos;

import java.util.List;

public class ListaInsertar {
  private List<ClienteInsertar> clientes;

  public ListaInsertar(List<ClienteInsertar> clientes) {
    this.clientes = clientes;
  }

  public List<ClienteInsertar> getClientes() { return clientes; }
  public void setClientes(List<ClienteInsertar> clientes) { this.clientes = clientes; }

  @Override
  public String toString() {
    return "clientes: " + clientes +
      "}";
  }
}
