package mx.baz.iyc.sdc.clasificador.mappers;

import mx.baz.iyc.sdc.clasificador.bean.nuevaRuta.AccionesRuta;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *  Clase que obtiene las acciones a validar la impresion en modulos.
 *  @author 202952 - Emmert - shernandeze@eleketra.com.mx
 *  @since 16-Junio-2021
 */
public class AccionesRowMapper implements RowMapper<AccionesRuta> {
  @Override
  public AccionesRuta mapRow(ResultSet rs, int rowNum) throws SQLException {
    AccionesRuta c1 = new AccionesRuta();
    c1.setAccion(rs.getInt(1));
    return c1;
  }
}
