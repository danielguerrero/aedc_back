package mx.baz.iyc.sdc.clasificador.controller;

import mx.baz.iyc.enums.Nivel;
import mx.baz.iyc.responsecontainer.model.ResponseService;
import mx.baz.iyc.sdc.clasificador.bean.segmentacion.Arbol;
import mx.baz.iyc.sdc.clasificador.dao.ArbolDAO;
import mx.baz.iyc.sdc.clasificador.dao.LeerClientesDAO;
import mx.baz.iyc.sdc.clasificador.model.ClientesDB;
import mx.baz.iyc.sdc.clasificador.service.ClientesSegmentacion;
import mx.baz.iyc.sdc.clasificador.utilerias.constantes.Endpoint;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.CodigoResponse;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.Folio;
import mx.baz.iyc.sdc.clasificador.utilerias.global.VariablesGlobales;
import mx.baz.iyc.service.EscribirLog;
import org.jvnet.staxex.BinaryText;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@RestController
@CrossOrigin(origins = "http://localhost:8083", methods= {RequestMethod.GET,RequestMethod.POST})
public class ClientesController {
  @Autowired
  private Folio folio;

  @Autowired
  private ClientesSegmentacion clienteSegmenta;

  @Autowired
  private LeerClientesDAO leerClientesDAO;

  @Autowired
  private ArbolDAO arbolDAO;

  @Autowired
  private EscribirLog escribirLog;

  private Arbol arbol;

  //Nuevo
  @RequestMapping(method = RequestMethod.GET, value = Endpoint.CLIENTES, produces = MediaType.APPLICATION_JSON_VALUE)
  private Object obtienerClientes() throws Exception {
    List<ClientesDB> c1 = leerClientesDAO.leerClientesNuevo();
    return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_200, folio.getFolio(), Arrays.asList(c1)),CodigoResponse.CODIGO_200.getHttpStatus());
  }

  @GetMapping(value = Endpoint.NUEVA_SEGMENTACION, produces = MediaType.APPLICATION_JSON_VALUE)
  public Object nuevaSegmentacion() throws Exception{
    Instant INICIO = Instant.now();

    escribirLog.escribir(ClientesController.class, Nivel.INFORMATIVO, "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    escribirLog.escribir(ClientesController.class, Nivel.INFORMATIVO, "Iniciamos ClientesController: " );

    arbol = clienteSegmenta.obtenerArbol();

    ExecutorService executor = Executors.newCachedThreadPool();
    List<Callable<String>> tareas = new ArrayList<>();

    Callable<String> tarea= ()->{
      // int numeroTarea = contadorTareas++;
      try {
        TimeUnit.MILLISECONDS.sleep(300);
        //Log("Inicio de la tarea " + numeroTarea);
        // ESTO LO CAMBIE
        // boolean resultado = clienteSegmenta.segmenta(arbol);
        boolean bandera = true;
        procesoVariablesLocales();

        while (bandera){
          int resultado = clienteSegmenta.segmentaNuevoFinal(arbol);
          bandera = validarBandera();
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      //Log("Finaliza la tarea " + numeroTarea);
      return "Resultado de la tarea";
    };

    tareas.add(tarea);
    tareas.add(tarea);
    tareas.add(tarea);
    tareas.add(tarea);
/*    tareas.add(tarea);
    tareas.add(tarea);
    tareas.add(tarea);*/

    Instant inicio= Instant.now();
    executor.invokeAll(tareas);
    // Log(Duration.between(inicio, Instant.now()));
    executor.shutdown();

    List<String> duraciones = new ArrayList<>();

    String duracion = Duration.between(INICIO, Instant.now()).toString();
    duraciones.add(duracion);

    return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_200, folio.getFolio(), Arrays.asList(duraciones)),CodigoResponse.CODIGO_200.getHttpStatus());
  }


  @GetMapping(value = Endpoint.SEGMENTACION, produces = MediaType.APPLICATION_JSON_VALUE)
  public Object nuevoArbol() throws InterruptedException, Exception {
    Instant INICIO = Instant.now();

    escribirLog.escribir(ClientesController.class, Nivel.INFORMATIVO, "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    escribirLog.escribir(ClientesController.class, Nivel.INFORMATIVO, "Inicio: " + INICIO);

    arbol = clienteSegmenta.obtenerArbol();

    boolean resultado = clienteSegmenta.segmentaNuevo(arbol);
    while (resultado) {
      resultado = clienteSegmenta.segmentaNuevo(arbol);
    }

    /*
    ExecutorService executor = Executors.newCachedThreadPool();

    List<Callable<String>> tareas = new ArrayList<>();

    Callable<String> tarea= ()->{
      int numeroTarea = contadorTareas++;
      try {
        TimeUnit.SECONDS.sleep(numeroTarea);
        //Log("Inicio de la tarea " + numeroTarea);
        boolean resultado = clienteSegmenta.segmentaNuevo(arbol);
        while (resultado) {
          resultado = clienteSegmenta.segmentaNuevo(arbol);
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      //Log("Finaliza la tarea " + numeroTarea);
      return "Resultado de la tarea";
    };

    tareas.add(tarea);
    tareas.add(tarea);
    tareas.add(tarea);
    tareas.add(tarea);
    tareas.add(tarea);
    tareas.add(tarea);
    tareas.add(tarea);

    Instant inicio= Instant.now();
    executor.invokeAll(tareas);
    Log(Duration.between(inicio, Instant.now()));

    executor.shutdown();
    */
    List<String> duraciones = new ArrayList<>();

    String duracion = Duration.between(INICIO, Instant.now()).toString();
    duraciones.add(duracion);

    return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_200, folio.getFolio(), Arrays.asList(duraciones)),CodigoResponse.CODIGO_200.getHttpStatus());
  }

  private static void Log(Object mensaje) {
    System.out.println(String.format("%s", mensaje.toString()));
  }

  @PostMapping(value = "/pruebas", produces = MediaType.APPLICATION_JSON_VALUE)
  public Object prueba() throws Exception{
    return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_200, folio.getFolio(), Arrays.asList("Todo ok")),CodigoResponse.CODIGO_200.getHttpStatus());
  }

  public void procesoVariablesLocales(){
    VariablesGlobales.putCache("bandera", 1);
  }

  public boolean validarBandera() {
    Map<String, Integer> map = VariablesGlobales.cache;
    int a = map.get("bandera");
/*    System.out.println("Valor de bandera: " + a);*/
    if (a == 0) { return false; }

    return true;
  }
}
