package mx.baz.iyc.sdc.clasificador.bean.admin;

/**
 * @author 202952 - Daniel Guerrero - dguerreror@elektra.com.mx
 * @since 18/MAYO/2021
 */

public class Empleado {
    private String noEmpleado;

    public String getNoEmpleado() { return noEmpleado; }
    public void setNoEmpleado(String noEmpleado) { this.noEmpleado = noEmpleado; }
}
