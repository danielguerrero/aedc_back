package mx.baz.iyc.sdc.clasificador.model.admin;

public class SalidaUnidadesNegocio {
    private String fiid;
    private String fcnombre;

    public String getFiid() { return fiid; }
    public void setFiid(String fiid) { this.fiid = fiid; }

    public String getFcnombre() { return fcnombre; }
    public void setFcnombre(String fcnombre) { this.fcnombre = fcnombre; }
}
