package mx.baz.iyc.sdc.clasificador.bean.segmentacion;

public class ClientesBean {
  int id;
  int[] caracterisiticas;

  public ClientesBean(int id, int[] caracterisiticas) {
    this.id = id;
    this.caracterisiticas = caracterisiticas;
  }

  public int getId() { return id; }
  public void setId(int id) { this.id = id; }

  public int[] getCaracterisiticas() { return caracterisiticas; }
  public void setCaracterisiticas(int[] caracterisiticas) { this.caracterisiticas = caracterisiticas; }
}
