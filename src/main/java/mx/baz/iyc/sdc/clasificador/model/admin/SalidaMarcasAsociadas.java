package mx.baz.iyc.sdc.clasificador.model.admin;

public class SalidaMarcasAsociadas {
    private String fiid;
    private String fcnombre;
    private String fnmarca;

    public String getFiid() { return fiid; }
    public void setFiid(String fiid) { this.fiid = fiid; }

    public String getFcnombre() { return fcnombre; }
    public void setFcnombre(String fcnombre) { this.fcnombre = fcnombre; }

    public String getFnmarca() { return fnmarca; }
    public void setFnmarca(String fnmarca) { this.fnmarca = fnmarca; }
}
