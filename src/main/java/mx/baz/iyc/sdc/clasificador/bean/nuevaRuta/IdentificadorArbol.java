package mx.baz.iyc.sdc.clasificador.bean.nuevaRuta;

public class IdentificadorArbol {
  private int fiid;
  private String fcnombre;
  
  public int getFiid() { return fiid; }
  public void setFiid(int fiid) { this.fiid = fiid; }

  public String getFcnombre() { return fcnombre; }
  public void setFcnombre(String fcnombre) { this.fcnombre = fcnombre; }
}
