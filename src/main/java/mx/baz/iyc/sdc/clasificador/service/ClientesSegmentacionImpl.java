package mx.baz.iyc.sdc.clasificador.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import mx.baz.iyc.enums.Nivel;
import mx.baz.iyc.sdc.clasificador.bean.nuevos.ClienteInsertar;
import mx.baz.iyc.sdc.clasificador.bean.nuevos.ListaInsertar;
import mx.baz.iyc.sdc.clasificador.bean.segmentacion.*;
import mx.baz.iyc.sdc.clasificador.controller.ClientesController;
import mx.baz.iyc.sdc.clasificador.dao.ArbolDAO;
import mx.baz.iyc.sdc.clasificador.dao.InsertarDAO;
import mx.baz.iyc.sdc.clasificador.dao.LeerClientesDAO;
import mx.baz.iyc.sdc.clasificador.model.ClientesDB;
import mx.baz.iyc.sdc.clasificador.model.SalidaEstandar;
import mx.baz.iyc.sdc.clasificador.utilerias.global.VariablesGlobales;
import mx.baz.iyc.service.EscribirLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

import static java.lang.Integer.parseInt;


@Component
public class ClientesSegmentacionImpl implements ClientesSegmentacion{

  @Autowired
  private LeerClientesDAO leerClientesDAO;

  @Autowired
  private ArbolDAO arbolDAO;

  @Autowired
  private InsertarDAO insertarDAO;

  @Autowired
  private EscribirLog escribirLog;

  public Callable<Object> getProcesaArbol() {
    return Executors.callable(()->{
      try {
        Arbol arbol = obtenerArbol();
        boolean resultado = segmentaNuevo(arbol);
        while (resultado){
          resultado = segmentaNuevo(arbol);
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      } catch (Exception e) {
        e.printStackTrace();
      }
    });
  }

/*  public boolean segmenta (Arbol arbol) throws Exception{
    int i;
    int band = 0;

    Bloque bloque = llenarClientes();

    List<ClientesBean> clientes = bloque.getClientes();

    System.out.println("Tamaño de clientes " + clientes.size() + " , bloque : " + bloque.getIdBloque());

    if(clientes.isEmpty() || clientes == null){ return false; }

    String resultado;
    StringBuilder impresion = new StringBuilder("{clientes: [");

    for (ClientesBean cli : clientes) {
      resultado = searchHoja(arbol.getChildren(), cli);
      band = 0;
      if (resultado == "Hoja sin final" || resultado == "No hay tipo equivalente") {
        band = 1;
      }
      impresion.append("{\"fiidcte\": " + cli.getId() + ", \"fnstatus\": " + band + ", \"clasificaciones\": \"{\\\"idClasificacion1\\\": " + resultado + "}\" },");
    }

    impresion.append("]}");

    SalidaEstandar salida = insertarDAO.guardarClientes(bloque.getIdBloque(), impresion.toString(), "193413");
    return true;
  }*/

  public boolean segmentaNuevo (Arbol arbol) throws Exception {
    int i;
    int band = 0;
    String resultado;

    Bloque bloque = llenarClientes();
    int idBloquecillo = bloque.getIdBloque();
    boolean validar = validaciones(idBloquecillo);

    if(idBloquecillo == -999) return true;

    List<ClientesBean> clientes = bloque.getClientes();

    escribirLog.escribir(ClientesController.class, Nivel.INFORMATIVO, "Bloque: " + idBloquecillo + " Validar: " + validar);

    List<ClienteInsertar> listaClientes = new ArrayList<>();

    for (ClientesBean cli : clientes) {
      resultado = searchHoja(arbol.getChildren(), cli);
      band = 0;
      if (resultado.equals("Hoja sin final") || resultado.equals("No hay tipo equivalente")) {
        band = 1;
      }
      listaClientes.add(new ClienteInsertar(cli.getId(), band, resultado));
    }

    ListaInsertar li = new ListaInsertar(listaClientes);

    String json = new Gson().toJson(li);
    SalidaEstandar salida = insertarDAO.guardarClientes(bloque.getIdBloque(), json, "193413");
    return true;
  }

  public int segmentaNuevoFinal (Arbol arbol) throws Exception {
    int i;
    int band = 0;
    String resultado;

    Bloque bloque = llenarClientes();
    int idBloquecillo = bloque.getIdBloque();
    boolean validar = validaciones(idBloquecillo);

    // Este es el caso raro
    if(idBloquecillo == -999) return 0;

    List<ClientesBean> clientes = bloque.getClientes();



    escribirLog.escribir(ClientesController.class, Nivel.INFORMATIVO, "Bloque: " + idBloquecillo + " Validar: " + validar);

    List<ClienteInsertar> listaClientes = new ArrayList<>();

    for (ClientesBean cli : clientes) {
      resultado = searchHoja(arbol.getChildren(), cli);
      band = 0;
      if (resultado.equals("Hoja sin final") || resultado.equals("No hay tipo equivalente")) {
        band = 1;
      }
      listaClientes.add(new ClienteInsertar(cli.getId(), band, resultado));
    }

    ListaInsertar li = new ListaInsertar(listaClientes);

    String json = new Gson().toJson(li);
    SalidaEstandar salida = insertarDAO.guardarClientes(bloque.getIdBloque(), json, "193413");
    return salida.getCodigo();
  }

  public boolean validaciones(int idBloquecillo){

    if(idBloquecillo == -999){
      escribirLog.escribir(ClientesController.class, Nivel.INFORMATIVO, "Este es el caso raro...");
      return true;
    }

    // Validacion para terminar este show
/*    if(idBloquecillo < 0) {
      return false;
    }*/

    // QUITAR ESTE CUANDO ESTE TODO CHINGON
/*    if(clientes == null || clientes.size() == 0 ||clientes.isEmpty()){
      System.out.println("Entramos a la validacion del mal");
      return true;
    }*/

    return true;
  }

  public String searchHoja (List<Arbol> rama, ClientesBean clien) {
    String resultado = "Hoja sin final";
    boolean brakancho = false;
    for(Arbol r : rama) {
      switch (r.getTipo()) {
        case -1:
          resultado = r.getName();
          break;
        case 1:
          if(r.getDefCaracterisitica()[0] == clien.getCaracterisiticas()[r.getIdCaract()]
            || clien.getCaracterisiticas()[r.getIdCaract()] == 2) {
            resultado = searchHoja(r.getChildren(), clien);
          }
          break;
        case 2:
          if(r.getDefCaracterisitica()[0] <= clien.getCaracterisiticas()[r.getIdCaract()]
            && r.getDefCaracterisitica()[1] >= clien.getCaracterisiticas()[r.getIdCaract()]) {
            resultado = searchHoja(r.getChildren(), clien);
          }
          break;
        default:
          resultado = "No hay tipo equivalente";
      }
    }
    return resultado;
  }

  public Arbol obtenerArbol() throws Exception{
    String prueba = arbolDAO.obtenerArbol().getCadArbol();
    Arbol jobj = new Gson().fromJson(prueba, Arbol.class);
    return jobj;
  }

  public Bloque llenarClientes() throws Exception{
    List<ClientesDB> clientesDB = leerClientesDAO.leerClientesNuevo();
    List<ClientesBean> clientes = new ArrayList<>();

    // escribirLog.escribir(ClientesSegmentacionImpl.class, Nivel.INFORMATIVO, "Tomamos: " + clientesDB.size());

    Bloque bloque = new Bloque();
    String id = "";
    Map<String, Integer> caracteristicas = null;
    int bloquecillo = 0;

    // Esta es la condicion que para todo esto
    if(bloquecillo < 0){
      VariablesGlobales.putCache("bandera", 0);
      return bloque;
    }

    if(clientes != null && clientesDB.size() > 0){
      // Caso normal
      String[] palabrasSeparadas = clientesDB.get(0).getBloque().split("\\|");
      bloquecillo = parseInt(palabrasSeparadas[0]);
    } else {
      // Caso anormal
      bloque.setIdBloque(-999);
      return bloque;
    }

    /*if(clientesDB.size() > 0 && bloquecillo > 0) {*/
    if(bloquecillo > 0){
      bloque.setIdBloque(bloquecillo);

      for (ClientesDB cliente : clientesDB) {
        id = cliente.getFiidcte();

        if(cliente.getFcctesvargral() == null || cliente.getFcctesvargral().isEmpty()){
          // escribirLog.escribir(ClientesSegmentacionImpl.class, Nivel.INFORMATIVO, "ClienteError: " + id);
        } else {
          caracteristicas = toJson(cliente.getFcctesvargral());
          clientes.add(dameClientesNuevo(id, caracteristicas));
        }
      }
    } else {
      System.out.println("Tenemos un bloquecillo extraño");
      VariablesGlobales.putCache("bandera", 0);
      //bloque.setIdBloque(-1500);
    }
    bloque.setClientes(clientes);
    return bloque;
  }

  public ClientesBean dameClientes(String id, JsonObject obj) throws Exception{
    escribirLog.escribir(ClientesSegmentacionImpl.class, Nivel.INFORMATIVO, "id: " + id + " | " + "JsonObject: " + obj);
    String tipoDepartamento = obj.get("tipoDepartamento").getAsString();
    String diasAtraso = obj.get("diasAtraso").getAsString();
    String score = obj.get("score").getAsString();
    String segmento = obj.get("segmento").getAsString();
    String productoF = obj.get("productoFraude").getAsString();
    String productoR = obj.get("productoRescate").getAsString();

    int[] arreglo = new int[6];
    arreglo[0] = parseInt(tipoDepartamento);
    arreglo[1] = parseInt(diasAtraso);
    arreglo[2] = parseInt(score);
    arreglo[3] = parseInt(segmento);
    arreglo[4] = parseInt(productoR);
    arreglo[5] = parseInt(productoF);

    ClientesBean cliente = new ClientesBean(parseInt(id), arreglo);
    return cliente;
  }

  /*
   *  25-JUN-2021 - Daniel Guerrero - dguerreror@elektra.com.mx
   *  Metodo creado como copia para mejorar el proceso de DameClientes anterior, que fallaba en situaciones muy especificas
   */
  public ClientesBean dameClientesNuevo(String id, Map<String, Integer> map) throws Exception{

    int[] arreglo = new int[6];
    arreglo[0] = map.get("tipoDepartamento");
    arreglo[1] = map.get("diasAtraso");
    arreglo[2] = map.get("score");
    arreglo[3] = map.get("segmento");
    arreglo[4] = map.get("productoRescate");
    arreglo[5] = map.get("productoFraude");

    ClientesBean cliente = new ClientesBean(parseInt(id), arreglo);
    return cliente;
  }


  public List<Caracteristicas> obtenerCaracteristicas (String noEmpleado) throws Exception{
    List<Caracteristicas> caract;
    caract = arbolDAO.obtenerCaracterísticas(noEmpleado);
    for(Caracteristicas cara : caract) {
      cara.setDatos(arbolDAO.obtenerDefiniciones(cara.getId()));
    }
    return caract;
  }

  /*
   *  25-JUN-2021 - Daniel Guerrero - dguerreror@elektra.com.mx
   *  Metodo creado para tratar de hacer una implementacion general de la conversion de objetos Java a JSON
   */

  public Map<String, Integer> toJson(String s) throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    mapper.enable(JsonReadFeature.ALLOW_LEADING_ZEROS_FOR_NUMBERS.mappedFeature());

    Map<String, Integer> resultado = null;

    if(s == null || s.isEmpty()) return resultado;

    // Deserializandolo a un map
    resultado = mapper.readValue(s, Map.class);

    return resultado;
  }
}
