package mx.baz.iyc.sdc.clasificador.dao.admin;

import mx.baz.iyc.enums.Nivel;
import mx.baz.iyc.sdc.clasificador.config.db.Functions;
import mx.baz.iyc.sdc.clasificador.config.db.OracleDao;
import mx.baz.iyc.sdc.clasificador.controller.ClientesController;
import mx.baz.iyc.sdc.clasificador.mappers.admin.ObtenerMarcasMapper;
import mx.baz.iyc.sdc.clasificador.mappers.admin.ObtenerMarcasUnidadMapper;
import mx.baz.iyc.sdc.clasificador.model.admin.SalidaMarcas;
import mx.baz.iyc.sdc.clasificador.model.admin.SalidaMarcasAsociadas;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.Folio;
import mx.baz.iyc.service.EscribirLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class ObtenerMarcasDAO {
    private final DataSource dataSource;

    @Autowired
    private Folio folio;

    @Autowired
    private EscribirLog escribirLog;

    @Autowired
    public ObtenerMarcasDAO(DataSource dataSource){ this.dataSource = dataSource; }

    public List<SalidaMarcasAsociadas> obtenerMarcasAsociadasPorUnidadNegocio() throws Exception{
        long idTransaction = folio.getFolioL();
        Map<String, Object> entrada = new LinkedHashMap<>();

        OracleDao<SalidaMarcasAsociadas> oracleDao = new OracleDao<>();
        escribirLog.escribir(ObtenerMarcasDAO.class, Nivel.INFORMATIVO, "Funcion: " + Functions.OBTENER_MARCAS_ASOCIADAS_POR_UNIDAD_NEGOCIO.getFuncion());

        List<SalidaMarcasAsociadas> salida = oracleDao.call(dataSource, entrada, Functions.OBTENER_MARCAS_ASOCIADAS_POR_UNIDAD_NEGOCIO.getFuncion(), new ObtenerMarcasUnidadMapper(), idTransaction);

        return salida;
    }

    public List<SalidaMarcas> obtenerMarcas() throws Exception{
        long idTransaction = folio.getFolioL();
        Map<String, Object> entrada = new LinkedHashMap<>();

        OracleDao<SalidaMarcas> oracleDao = new OracleDao<>();
        escribirLog.escribir(ObtenerMarcasDAO.class, Nivel.INFORMATIVO, "Funcion: " + Functions.OBTENER_MARCAS.getFuncion());

        List<SalidaMarcas> salida = oracleDao.call(dataSource, entrada, Functions.OBTENER_MARCAS.getFuncion(), new ObtenerMarcasMapper(), idTransaction);

        return salida;
    }
}
