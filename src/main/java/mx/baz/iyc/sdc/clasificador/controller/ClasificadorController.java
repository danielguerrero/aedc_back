package mx.baz.iyc.sdc.clasificador.controller;

import mx.baz.iyc.responsecontainer.model.ResponseService;
import mx.baz.iyc.sdc.clasificador.utilerias.constantes.Endpoint;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.CodigoResponse;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.Folio;
import mx.baz.iyc.service.EscribirLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

/**
 * @author 202952 - Daniel Guerrero - dguerreror@elektra.com.mx
 * @since 18/MAYO/2021
 */

@RestController
public class ClasificadorController {
    @Autowired
    private Folio folio;

    @Autowired
    private EscribirLog escribirLog;

    @RequestMapping(
            method = RequestMethod.GET,
            value = Endpoint.V1,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Object inicio() throws Exception{
        return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_200, folio.getFolio(), Arrays.asList("resultado")),CodigoResponse.CODIGO_200.getHttpStatus());
    }
}
