package mx.baz.iyc.sdc.clasificador.mappers;

import mx.baz.iyc.sdc.clasificador.bean.nuevaRuta.AccionesRuta;
import mx.baz.iyc.sdc.clasificador.bean.nuevaRuta.IdentificadorArbol;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *  Clase que obtiene los arboles a seleccionar.
 *  @author 202952 - Emmert - shernandeze@eleketra.com.mx
 *  @since 16-Junio-2021
 */
public class IdArbolRowMapper implements RowMapper<IdentificadorArbol> {
  @Override
  public IdentificadorArbol mapRow(ResultSet rs, int rowNum) throws SQLException {
    IdentificadorArbol c1 = new IdentificadorArbol();
    c1.setFiid(rs.getInt(1));
    c1.setFcnombre(rs.getString(2));
    return c1;
  }
}
