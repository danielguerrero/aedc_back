package mx.baz.iyc.sdc.clasificador.config.db;

/**
 * Clase con las funciones de base de datos utilizadas
 * @author 202952 - Daniel Guerrero - dguerreror@elektra.com.mx
 */

public enum Functions {
    // Funcion que sirve para extraer los clientes
    LEER_CLIENTES("RCREDITO.FNPROCESO"),

    // Funcion para insertar a los clientes
    //public static final String GUARDAR_CLIENTES = "RCREDITO.FNGUARDASEGME2";
    GUARDAR_CLIENTES("RCREDITO.FNGUARDASEGMEN"),

    // Funcion para obtener las características a seleccionar en el arbol
    // LEER_CARACTERISTICAS("RCREDITO.FNGETREGBRMS"),
    LEER_CARACTERISTICAS("RCREDITO.FNCRADLS1278"),
    // LEER_DEFINICIONES("RCREDITO.FNDEFINICION"),
    LEER_DEFINICIONES("RCREDITO.FNCRADLS1281"),
    // LEER_ACCIONES("RCREDITO.FNACCIONESSEG"),
    LEER_ACCIONES("RCREDITO.FNCRADLS1279"),
    // LEER_IDENTARBOL("RCREDITO.FNTIPOSARBOL"),
    LEER_IDENTARBOL("RCREDITO.FNCRADLS1280"),
    // LEER_ARBOL("RCREDITO.FNOBTENERARBOL"),
    LEER_ARBOL("RCREDITO.FNOBTENERARBOL"),
    // Funcion para guardar los arboles de decision
    GUARDAR_ARBOL("RCREDITO.FNCRADLG1274"),
    // Funcion para los arboles de decision
    // OBTENER_ARBOL("RCREDITO.FNOBTENERARBOL"),
    OBTENER_ARBOL("RCREDITO.FNOBTENERARBOL"),
    // Funcion de consulta de cobranza por gerencia
    CONSULTA_COBRANZA_GERENCIA("RCREDITO.FNCRADLS0932"),
    //Funcionues nuevas del serch
    OBTENER_UNIDADES_DE_NEGOCIO_POR_USUARIO("RCREDITO.FNCRADLS1288"),
    OBTENER_MARCAS_ASOCIADAS_POR_UNIDAD_NEGOCIO("RCREDITO.FNCRADLS1289"),
    OBTENER_MARCAS("RCREDITO.FNCRADLS1291");

    private String funcion;

    Functions(String funcion) {
        this.funcion = funcion;
    }

    public String getFuncion() { return funcion; }
}
