package mx.baz.iyc.sdc.clasificador.model.admin;

public class SalidaMarcas {
  private int fiid;
  private String fcnombre;

  public int getFiid() {
    return fiid;
  }

  public void setFiid(int fiid) {
    this.fiid = fiid;
  }

  public String getFcnombre() {
    return fcnombre;
  }

  public void setFcnombre(String fcnombre) {
    this.fcnombre = fcnombre;
  }
}
