package mx.baz.iyc.sdc.clasificador.mappers;


import mx.baz.iyc.sdc.clasificador.bean.segmentacion.CadArbol;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *  Clase que obtiene el arbol
 *  @author 202952 - Serchi - shernandeze@eleketra.com.mx
 *  @since 27-MAYO-2021
 */
public class LeerArbolRowMapper implements RowMapper<CadArbol> {

  @Override
  public CadArbol mapRow(ResultSet rs, int rowNum) throws SQLException {
    CadArbol c1 = new CadArbol();
    c1.setCadArbol(rs.getString(1));
    return c1;
  }

}
