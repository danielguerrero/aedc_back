package mx.baz.iyc.sdc.clasificador.bean.segmentacion;

import java.util.List;

public class Bloque {
  private int idBloque;
  private List<Integer> listBloques;
  private List<ClientesBean> clientes;

  public int getIdBloque() { return idBloque; }
  public void setIdBloque(int idBloque) { this.idBloque = idBloque; }

  public List<ClientesBean> getClientes() { return clientes; }
  public void setClientes(List<ClientesBean> clientes) { this.clientes = clientes; }

  public List<Integer> getListBloques() {
    return listBloques;
  }
  public void setListBloques(List<Integer> listBloques) {
    this.listBloques = listBloques;
  }
}
