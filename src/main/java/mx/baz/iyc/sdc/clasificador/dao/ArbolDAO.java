package mx.baz.iyc.sdc.clasificador.dao;

import mx.baz.iyc.sdc.clasificador.bean.segmentacion.CadArbol;
import mx.baz.iyc.sdc.clasificador.bean.segmentacion.Caracteristicas;
import mx.baz.iyc.sdc.clasificador.bean.segmentacion.Definicion;
import mx.baz.iyc.sdc.clasificador.config.db.Functions;
import mx.baz.iyc.sdc.clasificador.config.db.OracleDao;
import mx.baz.iyc.sdc.clasificador.mappers.CaracteristicasRowMapper;
import mx.baz.iyc.sdc.clasificador.mappers.DefinicionRowMapper;
import mx.baz.iyc.sdc.clasificador.mappers.InsertarArbolRowMapper;
import mx.baz.iyc.sdc.clasificador.mappers.LeerArbolRowMapper;
import mx.baz.iyc.sdc.clasificador.model.SalidaEstandar;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.Folio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Clase que sirve para acceder a los datos de cobranza
 * @author 202952 - Daniel Guerrero - dguerreror@elektra.com.mx
 * @since 27-MAYO-2021
 */

@Component
public class ArbolDAO {
  private final DataSource dataSource;

  @Autowired
  private Folio folio;

  @Autowired
  public ArbolDAO(DataSource dataSource){ this.dataSource = dataSource; }

  public SalidaEstandar insertarArbol(int opcion, int fiid, String nombre, String inicio, String fin, int status, String arbol, int tipo, int usuario, Date fecha) throws Exception {
    long idTransaction = folio.getFolioL();
    Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(inicio);
    Date date2=new SimpleDateFormat("dd/MM/yyyy").parse(fin);
    Map<String, Object> entrada = new LinkedHashMap<>();
    entrada.put("pa_opcion", opcion);
    entrada.put("pa_fiid", fiid);
    entrada.put("nombre", nombre);
    entrada.put("inicio", date1);
    entrada.put("fin", date2);
    entrada.put("pa_fistastus", status);
    entrada.put("pa_fblobarbol", arbol);
    entrada.put("tipo", tipo);
    entrada.put("pa_usuario_modifico", 193413);
    entrada.put("fecha", fecha);


    OracleDao<SalidaEstandar> oracleDao = new OracleDao<>();
    List<SalidaEstandar> salida = new ArrayList<>();

    salida = oracleDao.call(dataSource, entrada, Functions.GUARDAR_ARBOL.getFuncion(), new InsertarArbolRowMapper(), idTransaction);

    return salida.get(0);
  }

  public CadArbol obtenerArbol() throws Exception{
    long idTransaction = folio.getFolioL();
    Map<String, Object> entrada = new LinkedHashMap<>();

    OracleDao<CadArbol> oracleDao = new OracleDao<>();
    List<CadArbol> salida = new ArrayList<>();
    entrada.put("PA_FIID", 1);

    salida = oracleDao.call(dataSource, entrada, Functions.OBTENER_ARBOL.getFuncion(), new LeerArbolRowMapper(), idTransaction);

    return salida.get(0);
  }
  public List<Caracteristicas> obtenerCaracterísticas (String usuario) throws Exception{
    long idTransaction = folio.getFolioL();
    Map<String, Object> entrada = new LinkedHashMap<>();

    OracleDao<Caracteristicas> oracleDao = new OracleDao<>();
    entrada.put("usuario", usuario);
    List<Caracteristicas> salida = new ArrayList<>();

    salida = oracleDao.call(dataSource, entrada, Functions.LEER_CARACTERISTICAS.getFuncion(), new CaracteristicasRowMapper(), idTransaction);

    return salida;
  }
  public List<Definicion>  obtenerDefiniciones (int idRegla) throws Exception{
    long idTransaction = folio.getFolioL();
    Map<String, Object> entrada = new LinkedHashMap<>();
    OracleDao<Definicion> oracleDao = new OracleDao<>();
    entrada.put("idRegla", idRegla);
    List<Definicion> salida = new ArrayList<>();

    salida = oracleDao.call(dataSource, entrada, Functions.LEER_DEFINICIONES.getFuncion(), new DefinicionRowMapper(), idTransaction);
    return salida;
  }
}
