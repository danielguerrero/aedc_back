package mx.baz.iyc.sdc.clasificador.model;

public class GuardarCliente {
  private int idBloque;
  private String cadena;
  private String empleado;

  public int getIdBloque() { return idBloque; }
  public void setIdBloque(int idBloque) { this.idBloque = idBloque; }

  public String getCadena() { return cadena; }
  public void setCadena(String cadena) { this.cadena = cadena; }

  public String getEmpleado() { return empleado; }
  public void setEmpleado(String empleado) { this.empleado = empleado; }
}
