package mx.baz.iyc.sdc.clasificador.bean;

import java.util.List;

/**
 * @author 202952 - Daniel Guerrero - dguerreror@elektra.com.mx
 * @since 18/MAYO/2021
 */


public class Cliente {
    private String id;
    private String segmentacion;
    private int diaDeAtraso;
    private List<Integer> fitires;
    private int focoAlFraude;
    private int productoRescate;

    public Cliente() {
    }

    public Cliente(String id, String segmentacion, int diaDeAtraso, List<Integer> fitires, int focoAlFraude, int productoRescate) {
        this.id = id;
        this.segmentacion = segmentacion;
        this.diaDeAtraso = diaDeAtraso;
        this.fitires = fitires;
        this.focoAlFraude = focoAlFraude;
        this.productoRescate = productoRescate;
    }

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public String getSegmentacion() { return segmentacion; }
    public void setSegmentacion(String segmentacion) { this.segmentacion = segmentacion; }

    public int getDiaDeAtraso() { return diaDeAtraso; }
    public void setDiaDeAtraso(int diaDeAtraso) { this.diaDeAtraso = diaDeAtraso; }

    public List<Integer> getFitires() { return fitires; }
    public void setFitires(List<Integer> fitires) { this.fitires = fitires; }

    public int getFocoAlFraude() { return focoAlFraude; }
    public void setFocoAlFraude(int focoAlFraude) { this.focoAlFraude = focoAlFraude; }

    public int getProductoRescate() { return productoRescate; }
    public void setProductoRescate(int productoRescate) { this.productoRescate = productoRescate; }
}
