package mx.baz.iyc.sdc.clasificador.controller;

import mx.baz.iyc.enums.Nivel;
import mx.baz.iyc.responsecontainer.model.ErrorDescription;
import mx.baz.iyc.responsecontainer.model.ResponseError;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.CodigoResponse;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.Folio;
import mx.baz.iyc.service.EscribirLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.InvalidResultSetAccessException;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.Arrays;

/**
 * Clase que sirve para atrapar algunos errores de forma general
 * @author 202952 - Daniel Guerrero - dguerreror@elektra.com.mx
 * @since 29-JUNIO-2021
 */

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice(annotations = {RestController.class})
public class UncaugthExceptionsControllerAdvice {
    @Autowired
    private Folio folio;

    @Autowired
    private EscribirLog escribirLog;

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity handleBindingErrors(Exception ex){
        ErrorDescription[] errores = {new ErrorDescription("Datos de entrada incorrectos", Arrays.asList("Datos de entrada incorrectos"))};

        escribirLog.escribir(getClass(), Nivel.ERROR, "Entrada de datos incorrecta: " + ex);

        ResponseError response = new ResponseError(CodigoResponse.CODIGO_400.getCodigo(),
                CodigoResponse.CODIGO_400.getDescripcion(), folio.getFolio(),
                "https://baz-developer.bancoazteca.com.mx/errors#"
                        + CodigoResponse.CODIGO_400.getCodigo(),
                Arrays.asList(errores));
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BadSqlGrammarException.class)
    public ResponseEntity handleBadSqlGrammarException(Exception ex){
        ErrorDescription[] errores = {new ErrorDescription("Problemas en la funcion", Arrays.asList("Datos de entrada incorrectos"))};

        escribirLog.escribir(getClass(), Nivel.ERROR, "Problemas en la funcion: " + ex);

        ResponseError response = new ResponseError(CodigoResponse.CODIGO_500.getCodigo(),
                CodigoResponse.CODIGO_500.getCodigo(), folio.getFolio(),
                "https://baz-developer.bancoazteca.com.mx/errors#"
                        + CodigoResponse.CODIGO_500.getCodigo(),
                Arrays.asList(errores));
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(InvalidResultSetAccessException.class)
    public ResponseEntity handleInvalidResultSetAccessException(Exception ex){
        ErrorDescription[] errores = {new ErrorDescription("Problemas en la funcion", Arrays.asList("Datos de entrada incorrectos"))};

        escribirLog.escribir(getClass(), Nivel.ERROR, "InvalidResultSetAccessException: " + ex);

        ResponseError response = new ResponseError(CodigoResponse.CODIGO_500.getDescripcion(),
                CodigoResponse.CODIGO_500.getDescripcion(), folio.getFolio(),
                "https://baz-developer.bancoazteca.com.mx/errors#"
                        + CodigoResponse.CODIGO_500.getCodigo(),
                Arrays.asList(errores));
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(CannotGetJdbcConnectionException.class)
    public ResponseEntity handleCannotGetJdbcConnectionException(Exception ex){
        ErrorDescription[] errores = {new ErrorDescription("Problemas en la conexion de la base de datos", Arrays.asList("Datos de entrada incorrectos"))};

        escribirLog.escribir(getClass(), Nivel.ERROR, "CannotGetJdbcConnectionException: " + ex);

        ResponseError response = new ResponseError(CodigoResponse.CODIGO_500.getDescripcion(),
                CodigoResponse.CODIGO_500.getDescripcion(), folio.getFolio(),
                "https://baz-developer.bancoazteca.com.mx/errors#"
                        + CodigoResponse.CODIGO_500.getCodigo(),
                Arrays.asList(errores));
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(UncategorizedSQLException.class)
    public ResponseEntity handleUncategorizedSQLException(Exception ex){
        ErrorDescription[] errores = {new ErrorDescription("funcion", Arrays.asList("Funcion incorrecta"))};

        escribirLog.escribir(getClass(), Nivel.ERROR, "UncategorizedSQLException: " + ex);

        ResponseError response = new ResponseError(CodigoResponse.CODIGO_400.getCodigo(),
                CodigoResponse.CODIGO_400.getDescripcion(), folio.getFolio(),
                "https://baz-developer.bancoazteca.com.mx/errors#"
                        + CodigoResponse.CODIGO_400.getCodigo(),
                Arrays.asList(errores));
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(SQLException.class)
    public ResponseEntity handleSQLException(Exception ex){
        ErrorDescription[] errores = {new ErrorDescription("Problemas en la funcion", Arrays.asList("Datos de entrada incorrectos"))};

        escribirLog.escribir(getClass(), Nivel.ERROR, "handleSQLException: " + ex);

        ResponseError response = new ResponseError(CodigoResponse.CODIGO_500.getDescripcion(),
                CodigoResponse.CODIGO_500.getDescripcion(), folio.getFolio(),
                "https://baz-developer.bancoazteca.com.mx/errors#"
                        + CodigoResponse.CODIGO_500.getCodigo(),
                Arrays.asList(errores));
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity handleMissingServletRequestParameterException(Exception ex){
        ErrorDescription[] errores = {new ErrorDescription("Datos de entrada incorrectos", Arrays.asList("Parametro necesitado no encontrado..."))};

        escribirLog.escribir(getClass(), Nivel.ERROR, "handleMissingServletRequestParameterException");

        ResponseError response = new ResponseError(CodigoResponse.CODIGO_400.getCodigo(),
                CodigoResponse.CODIGO_400.getDescripcion(), folio.getFolio(),
                "https://baz-developer.bancoazteca.com.mx/errors#"
                        + CodigoResponse.CODIGO_400.getCodigo(),
                Arrays.asList(errores));
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleException(Exception ex){
        ErrorDescription[] errores = {new ErrorDescription("Error en el servidor", Arrays.asList("Error desconocido"))};

        escribirLog.escribir(getClass(), Nivel.ERROR, "Error desconocido: " + ex.getMessage());

        ex.printStackTrace();

        ResponseError response = new ResponseError(CodigoResponse.CODIGO_500.getCodigo(),
                CodigoResponse.CODIGO_500.getDescripcion(), folio.getFolio(),
                "https://baz-developer.bancoazteca.com.mx/errors#"
                        + CodigoResponse.CODIGO_500.getCodigo(),
                Arrays.asList(errores));
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
