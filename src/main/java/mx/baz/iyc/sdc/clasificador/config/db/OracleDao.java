package mx.baz.iyc.sdc.clasificador.config.db;

import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

/**
 * Realiza la llamada a un StoreProcedure.
 * @author 347409 - Ubaldo Torres Juarez - utorresj@elektra.com.mx
 * @modified 202952 - Daniel Guerrero - dguerreror@elektra.com.mx
 * @since 17-ABRIL-2020
 */
public class OracleDao<E> extends StoredProcedure {

    private static final String CURSOR = "cursor";

    /**
     * Realiza la ejecucion de una funcion definiendo un Bean de propiedades para la salida.
     * @param dataSource Datasource a utilizar
     * @param in parametros de entrada
     * @param function nombre de la funcion
     * @param out clase del bean de salida
     * @param idTransaction identificador de la transaccion que realiza la consulta a BD
     * @return Lista de los elementos del cursor.
     */
    public List<E> call(DataSource dataSource, Map<String,Object> in, String function, Class out, long idTransaction){

        setDataSource(dataSource);
        setSql(function);
        setFunction(true);
        declareParameter(new SqlOutParameter(CURSOR, OracleTypes.CURSOR, new BeanPropertyRowMapper(out)));

        prepareInput(in);

        compile();
        final StringBuilder builder = new StringBuilder();
        in.values().forEach(value -> builder.append(value).append(','));

        return (List<E>) execute(in).get(CURSOR);
    }

    /**
     * Realiza la ejecucion de una funcion definiendo un RowMapper para la salida.
     * @param dataSource Datasource a utilizar
     * @param in parametros de entrada
     * @param function nombre de la funcion
     * @param rowMapper clase del bean de salida
     * @param idTransaction identificador de la transaccion que realiza la consulta a BD
     * @return Lista de los elementos del cursor.
     */
    public List<E> call(DataSource dataSource, Map<String,Object> in, String function, RowMapper<E> rowMapper, long idTransaction) throws Exception{
        setDataSource(dataSource);
        setSql(function);
        setFunction(true);
        declareParameter(new SqlOutParameter(CURSOR, OracleTypes.CURSOR, rowMapper));

        prepareInput(in);

        compile();

        final StringBuilder builder = new StringBuilder();
        in.values().forEach(value -> builder.append(value).append('-'));

        return (List<E>) execute(in).get(CURSOR);

    }

    /**
     * Prepara el input de los parametros de una funcion y determina que tipo de dato SQL de entrada es el objeto que
     * se analiza
     * @param in Parametros a evaluar y preparar.
     */
    private void prepareInput(Map<String, Object> in){
        for (Map.Entry<String, Object> entry : in.entrySet()){
            Object value = in.get(entry.getKey());
            declareParameter(new SqlParameter(entry.getKey(), OracleUtil.getType(value)));
        }
    }
}

