package mx.baz.iyc.sdc.clasificador.model;

public class ClientesDB {
  private String bloque;
  private String fiidcte;
  private String fcctesvargral;

  public String getBloque() { return bloque; }
  public void setBloque(String bloque) { this.bloque = bloque; }

  public String getFiidcte() { return fiidcte; }
  public void setFiidcte(String fiidcte) { this.fiidcte = fiidcte; }

  public String getFcctesvargral() { return fcctesvargral; }
  public void setFcctesvargral(String fcctesvargral) { this.fcctesvargral = fcctesvargral; }
}
