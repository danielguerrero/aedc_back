package mx.baz.iyc.sdc.clasificador.config;

import mx.baz.iyc.responsecontainer.config.ConfigurationResponse;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.CodigoResponse;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.Folio;
import mx.baz.iyc.utiljwt.StubLoggingFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

@Component
public class ApplicationSetup {

    private Map<String, String> urls;

    @Autowired
    private ConfigurationResponse confi;

    @Autowired
    private Folio folio;

    @Autowired
    private StubLoggingFilter stubLogginFilter;

    @Autowired
    private AppConfig appConfig;

    @PostConstruct
    private void init() {

        confi.setCodigosRespuesta(CodigoResponse.CODIGO_400);
        confi.setFolio(folio.getFolio());

        stubLogginFilter.agregarURLExcepciones("/ProyectoGradle/monitoreo/test");

        stubLogginFilter.setPublicKey(appConfig.getLlavePublica());

    }

    public Map<String, String> getUrls() {
        return urls;
    }

}
