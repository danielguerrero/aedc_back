package mx.baz.iyc.sdc.clasificador.utilerias.global;

import java.util.HashMap;
import java.util.Map;

public class VariablesGlobales {
    public static Map<String, Integer> cache = new HashMap<>();        // Variable global

    public static Map<String, Integer> getCache(){ return cache; }

    // Agregar valores
    public static void putCache(String key, Integer value){
        cache.put(key, value);
    }
}
