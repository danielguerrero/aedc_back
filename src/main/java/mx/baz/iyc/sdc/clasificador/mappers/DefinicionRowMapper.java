package mx.baz.iyc.sdc.clasificador.mappers;

import mx.baz.iyc.sdc.clasificador.bean.segmentacion.Definicion;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *  Clase que obtiene las características utilizadas para el arbol.
 *  @author 202952 - Emmert - shernandeze@eleketra.com.mx
 *  @since 10-Junio-2021
 */
public class DefinicionRowMapper implements RowMapper<Definicion> {
  @Override
  public Definicion mapRow(ResultSet rs, int rowNum) throws SQLException {
    Definicion c1 = new Definicion();
    c1.setValor(rs.getInt(1));
    c1.setName(rs.getString(2));
    return c1;
  }
}
