package mx.baz.iyc.sdc.clasificador.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import mx.baz.iyc.enums.Nivel;
import mx.baz.iyc.responsecontainer.model.ResponseService;
import mx.baz.iyc.sdc.clasificador.bean.InsertarArbol;
import mx.baz.iyc.sdc.clasificador.bean.nuevaRuta.AccionesRuta;
import mx.baz.iyc.sdc.clasificador.bean.nuevaRuta.IdentificadorArbol;
import mx.baz.iyc.sdc.clasificador.bean.segmentacion.CadArbol;
import mx.baz.iyc.sdc.clasificador.bean.segmentacion.Caracteristicas;
import mx.baz.iyc.sdc.clasificador.dao.ArbolDAO;
import mx.baz.iyc.sdc.clasificador.dao.ruta.INuevaRutaDAO;
import mx.baz.iyc.sdc.clasificador.model.SalidaEstandar;
import mx.baz.iyc.sdc.clasificador.service.ClientesSegmentacion;
import mx.baz.iyc.sdc.clasificador.utilerias.constantes.Endpoint;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.CodigoResponse;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.Folio;
import mx.baz.iyc.service.EscribirLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @author 193413 - Emmert S - shernandeze@elektra.com.mx
 * @since 28/julio/2021
 */
@RestController
@RequestMapping("/nuevaruta")
public class RutaController {
  @Autowired
  private Folio folio;

  @Autowired
  private INuevaRutaDAO nuevaRutaDAO;

  @Autowired
  private ArbolDAO arbolDAO;

  @Autowired
  private ClientesSegmentacion clienteSegmenta;

  @Autowired
  private EscribirLog escribirLog;

  @GetMapping(value = "/acciones/{noEmpleado}" , produces = MediaType.APPLICATION_JSON_VALUE)
  public Object obtenerAcciones(@PathVariable String noEmpleado)  throws Exception{
    List<AccionesRuta> acciones = new ArrayList();
    acciones = nuevaRutaDAO.obtenerAcciones(noEmpleado);
    return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_200, folio.getFolio(), Arrays.asList(acciones)),CodigoResponse.CODIGO_200.getHttpStatus());
  }
  @GetMapping(value = "/indicadores/{noEmpleado}" , produces = MediaType.APPLICATION_JSON_VALUE)
  public Object obtenerIdArbol (@PathVariable String noEmpleado)  throws Exception {
    List<IdentificadorArbol> identificadores = new ArrayList();
    identificadores = nuevaRutaDAO.obtenerIdArboles(noEmpleado);
    return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_200, folio.getFolio(), Arrays.asList(identificadores)),CodigoResponse.CODIGO_200.getHttpStatus());
  }
  @GetMapping(value="/arbol/{idArbol}", produces = MediaType.APPLICATION_JSON_VALUE)
  public Object obtenerArbol(@PathVariable int idArbol) throws Exception {
    CadArbol cadena = new CadArbol();
    cadena = nuevaRutaDAO.obtenerArbol(idArbol);
    String limpio = cadena.getCadArbol().replace("\\", "");
    JsonObject j = new Gson().fromJson(limpio, JsonObject.class);
    String encodedString = Base64.getEncoder().encodeToString(j.toString().getBytes());
    return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_200, folio.getFolio(), encodedString),CodigoResponse.CODIGO_200.getHttpStatus());
  }
  @GetMapping(value = "/caracteristicas/{noEmpleado}" , produces = MediaType.APPLICATION_JSON_VALUE)
  public Object obtencionCaracteristicas(@PathVariable String noEmpleado) throws Exception{
    List<Caracteristicas> respuesta = new ArrayList<>();
    respuesta = clienteSegmenta.obtenerCaracteristicas(noEmpleado);
    return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_200, folio.getFolio(), Arrays.asList(respuesta)),CodigoResponse.CODIGO_200.getHttpStatus());
  }
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = Endpoint.ARBOL, produces = MediaType.APPLICATION_JSON_VALUE)
  public Object guardarArbol(@RequestBody InsertarArbol arbol) throws Exception{
    Date d1 = new Date();

    // Decode data on other side, by processing encoded data
    byte[] valueDecoded = org.apache.commons.codec.binary.Base64.decodeBase64(arbol.getArbol());

    SalidaEstandar s1 = arbolDAO.insertarArbol(arbol.getOpcion(), arbol.getFiid(), arbol.getNombre(), arbol.getInicio(), arbol.getFin(), arbol.getStatus(), new String(valueDecoded), arbol.getTipo(), arbol.getUsuario(), d1);
    return new ResponseEntity(new ResponseService(CodigoResponse.CODIGO_200, folio.getFolio(), Arrays.asList("Todo ok")),CodigoResponse.CODIGO_200.getHttpStatus());
  }

}
