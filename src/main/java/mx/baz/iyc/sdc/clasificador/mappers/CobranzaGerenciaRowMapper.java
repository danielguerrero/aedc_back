package mx.baz.iyc.sdc.clasificador.mappers;

import mx.baz.iyc.sdc.clasificador.model.Cobranza;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *  Clase que mapea la salida del cursor en las funciones de Cobranza
 *  @author 202952 - Daniel Guerrero - dguerreror@eleketra.com.mx
 *  @since 03-ABRIL-2020
 */

public class CobranzaGerenciaRowMapper implements RowMapper<Cobranza> {

    @Override
    public Cobranza mapRow(ResultSet rs, int rowNum) throws SQLException {
        Cobranza cob = new Cobranza();

        cob.setCobranzaVigente(rs.getInt(2));
        cob.setCobranzaRelacional(rs.getInt(3));
        cob.setCobranzaContingencia(rs.getInt(4));
        cob.setCobranzaRiesgo(rs.getInt(5));
        cob.setCobranzaVencido(rs.getInt(6));

        return cob;
    }

}
