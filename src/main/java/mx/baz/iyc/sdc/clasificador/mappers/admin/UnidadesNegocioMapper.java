package mx.baz.iyc.sdc.clasificador.mappers.admin;

import mx.baz.iyc.sdc.clasificador.model.admin.SalidaUnidadesNegocio;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *  @author 202952 - Daniel Guerrero - dguerreror@eleketra.com.mx
 *  @since 29-JUNIO-2021
 */
public class UnidadesNegocioMapper implements RowMapper<SalidaUnidadesNegocio>{
    
    @Override
    public SalidaUnidadesNegocio mapRow(ResultSet rs, int rowNum) throws SQLException {
        SalidaUnidadesNegocio s = new SalidaUnidadesNegocio();
        s.setFiid(rs.getString(1));
        s.setFcnombre(rs.getString(2));
        return s;
    }
}
