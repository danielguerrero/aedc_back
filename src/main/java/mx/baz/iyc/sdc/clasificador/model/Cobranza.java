package mx.baz.iyc.sdc.clasificador.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Cobranza {
    @JsonProperty("vigente")
    private int cobranzaVigente;
    @JsonProperty("relacional")
    private int cobranzaRelacional;
    @JsonProperty("contingencia")
    private int cobranzaContingencia;
    @JsonProperty("riesgo")
    private int cobranzaRiesgo;
    @JsonProperty("vencido")
    private int cobranzaVencido;

    public int getCobranzaVigente() { return cobranzaVigente; }
    public void setCobranzaVigente(int cobranzaVigente) { this.cobranzaVigente = cobranzaVigente; }

    public int getCobranzaRelacional() { return cobranzaRelacional; }
    public void setCobranzaRelacional(int cobranzaRelacional) { this.cobranzaRelacional = cobranzaRelacional; }

    public int getCobranzaContingencia() { return cobranzaContingencia; }
    public void setCobranzaContingencia(int cobranzaContingencia) { this.cobranzaContingencia = cobranzaContingencia; }

    public int getCobranzaRiesgo() { return cobranzaRiesgo; }
    public void setCobranzaRiesgo(int cobranzaRiesgo) { this.cobranzaRiesgo = cobranzaRiesgo; }

    public int getCobranzaVencido() { return cobranzaVencido; }
    public void setCobranzaVencido(int cobranzaVencido) { this.cobranzaVencido = cobranzaVencido; }
}
