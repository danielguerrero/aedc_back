package mx.baz.iyc.sdc.clasificador.dao.ruta;

import mx.baz.iyc.sdc.clasificador.bean.nuevaRuta.AccionesRuta;
import mx.baz.iyc.sdc.clasificador.bean.nuevaRuta.IdentificadorArbol;
import mx.baz.iyc.sdc.clasificador.bean.segmentacion.CadArbol;
import mx.baz.iyc.sdc.clasificador.bean.segmentacion.Caracteristicas;
import mx.baz.iyc.sdc.clasificador.config.db.Functions;
import mx.baz.iyc.sdc.clasificador.config.db.OracleDao;
import mx.baz.iyc.sdc.clasificador.mappers.AccionesRowMapper;
import mx.baz.iyc.sdc.clasificador.mappers.CaracteristicasRowMapper;
import mx.baz.iyc.sdc.clasificador.mappers.IdArbolRowMapper;
import mx.baz.iyc.sdc.clasificador.mappers.LeerArbolRowMapper;
import mx.baz.iyc.sdc.clasificador.utilerias.enums.Folio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;
@Component
public class NuevaRutaDAO implements INuevaRutaDAO{
  private final DataSource dataSource;

  @Autowired
  private Folio folio;

  @Autowired
  public NuevaRutaDAO(DataSource dataSource){ this.dataSource = dataSource; }

  public List<AccionesRuta> obtenerAcciones(String noEmpleado) throws Exception{
    long idTransaction = folio.getFolioL();
    Map<String, Object> entrada = new LinkedHashMap<>();
    OracleDao<AccionesRuta> oracleDao = new OracleDao<>();
    entrada.put("noEmpleado", noEmpleado);
    List<AccionesRuta> salida = new ArrayList<>();
    salida = oracleDao.call(dataSource, entrada, Functions.LEER_ACCIONES.getFuncion(), new AccionesRowMapper(), idTransaction);
    return salida;
  }
  public List<IdentificadorArbol> obtenerIdArboles(String noEmpleado) throws Exception {
    long idTransaction = folio.getFolioL();
    Map<String, Object> entrada = new LinkedHashMap<>();
    OracleDao<IdentificadorArbol> oracleDao = new OracleDao<>();
    entrada.put("noEmpleado", noEmpleado);
    List<IdentificadorArbol> salida = new ArrayList<>();
    salida =  oracleDao.call(dataSource, entrada, Functions.LEER_IDENTARBOL.getFuncion(), new IdArbolRowMapper(),idTransaction);
    return salida;
  }
  public CadArbol obtenerArbol(int IdArbol) throws Exception{
    long idTransaction = folio.getFolioL();
    Map<String, Object> entrada = new LinkedHashMap<>();
    OracleDao<CadArbol> oracleDao = new OracleDao<>();
    entrada.put("IdArbol", IdArbol);
    List<CadArbol> salida = new ArrayList<>();
    salida = oracleDao.call(dataSource, entrada, Functions.LEER_ARBOL.getFuncion(), new LeerArbolRowMapper(), idTransaction);
    return salida.get(0);
  }
}
