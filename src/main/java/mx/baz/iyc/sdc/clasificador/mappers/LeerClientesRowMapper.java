package mx.baz.iyc.sdc.clasificador.mappers;

import mx.baz.iyc.sdc.clasificador.model.ClientesDB;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *  Clase que mapea la salida del cursor en las funciones de Cobranza
 *  @author 202952 - Daniel Guerrero  - dguerreror@eleketra.com.mx
 *  @author 193413 - Sergio Hernandez - shernandeze@elektra.com.mx
 *  @since 26-MAYO-2021
 */

public class LeerClientesRowMapper implements RowMapper<ClientesDB> {

  @Override
  public ClientesDB mapRow(ResultSet rs, int rowNum) throws SQLException {
    ClientesDB c1 = new ClientesDB();

    c1.setBloque(rs.getString(1));
    c1.setFiidcte(rs.getString(2));
    c1.setFcctesvargral(rs.getString(3));

    return c1;
  }
}